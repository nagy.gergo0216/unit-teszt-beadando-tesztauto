﻿using System;
using Assignment.Numbers;
using NUnit.Framework;

namespace AssigmentTests
{
    [TestFixture]
    public class NumberGeneratorTests
    {
        private NumberGenerator _numberGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            _numberGenerator=new NumberGenerator();
        }

        [TestCase(4)]
        [TestCase(6)]
        public void GenerateEvenNumber_ShouldReturnAnEvenNumber_WhenLimitIsWhenLimitIsAPositiveNumberAndBiggerThanThree(int limit)
        {
            //act
            var result = _numberGenerator.GenerateEven(limit);
            //assert
            Assert.AreEqual(0,result % 2);
        }
        [TestCase(-5)]
        [TestCase(-3)]
        [TestCase(-7)]
        public void GenerateEvenNumber_ShouldReturnArgumentOutOfRangeException_WhenLimitIsNegativeNumber(int limit)
        {
            //act
            //assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberGenerator.GenerateEven(limit));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void GenerateEvenNumber_ShouldReturnZero_WhenLimitIsGivenInTheParameter(int limit)
        {
            //act
            var result = _numberGenerator.GenerateEven(limit);
            //assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GenerateEvenNumber_ShouldReturnARandomNumber_WhenLimitIsAPossitiveNumberAndBiggerThanThree()
        {
            //act
            var result = _numberGenerator.GenerateEven(15);
            //assert
            Assert.That(result >= 0 && result <= 14);
        }

        [TestCase(5)]
        [TestCase(7)]
        public void GenerateOddNumber_ShouldReturnAnOddNumber_WhenLimitIsWhenLimitIsAPositiveNumberAndBiggerThanThree(int limit)
        {
         
            //act
            var result = _numberGenerator.GenerateOdd(limit);
            //assert
            Assert.AreEqual(1, result % 2);
        }

        [TestCase(-1)]
        [TestCase(-5)]
        public void GenerateOddNumber_ShouldReturnArgumentOutOfRangeException_WhenLimitIsWhenLimitIsANegativeNumber(int limit)
        {
            //act
            //assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberGenerator.GenerateEven(limit));
        }
    }
}
