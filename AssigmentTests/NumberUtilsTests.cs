﻿using System;
using System.Collections.Generic;
using Assignment.Numbers;
using NUnit.Framework;

namespace AssigmentTests
{
    [TestFixture]
    public class NumberUtilsTests
    {
        private NumberUtils _numberUtils;

        [OneTimeSetUp]
        public void Setup()
        {
            _numberUtils = new NumberUtils();
        }

        [TestCase(2)]
        [TestCase(4)]
        public void EvenOrOdd_ShouldReturnEvenString_WhenNumberIsEven(int number)
        {
            //act
            var result = _numberUtils.EvenOrOdd(number);
            //assert
            Assert.That(result, Is.EqualTo("even"));
        }

        [TestCase(1)]
        [TestCase(3)]
        public void EvenOrOdd_ShouldReturnOddString_WhenNumberIsEven(int number)
        {
            //act
            var result = _numberUtils.EvenOrOdd(number);
            //assert
            Assert.That(result, Is.EqualTo("odd"));
        }

        [Test]
        public void IsPrime_ShouldReturnFalse_WhenNumberIsNotPrime()
        {
            //act
            var result = _numberUtils.IsPrime(10);
            //assert
            Assert.That(result, Is.EqualTo(false));
        }

        [Test]
        public void IsPrime_ShouldReturnTrue_WhenNumberIsPrime()
        {
            //act
            var result = _numberUtils.IsPrime(3);
            //assert
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void GetDivisors_ShouldReturnTheDivisorsOfNumber_WhenNumberIsAPositiveNumber()
        {
            //act
            var result = _numberUtils.GetDivisors(10);
            //assert
            Assert.That(result, Is.EquivalentTo(new List<int>() { 1, 2, 5, 10 }));
        }

        [Test]
        public void GetDivisors_ShouldReturnArgumentOutOfRangeException_WhenNumberIsANegativeNumber()
        {
            //act
            //assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberUtils.GetDivisors(-2));
        }
    }

}

