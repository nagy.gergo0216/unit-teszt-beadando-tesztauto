﻿using System;
using System.Collections.Generic;
using Assignment.Numbers;
using Assignment.Strings;
using Moq;
using NUnit.Framework;

namespace AssigmentTests
{
    [TestFixture]
    public class StringGeneratorTests
    {
        private Mock<INumberGenerator> _mockedNumberGenerator;
        private StringGenerator _stringGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            _mockedNumberGenerator=new Mock<INumberGenerator>();

            _mockedNumberGenerator.Setup(x => x.GenerateEven(It.IsAny<int>())).Returns(2);
            _mockedNumberGenerator.Setup(x => x.GenerateOdd(It.IsAny<int>())).Returns(9);
            _stringGenerator=new StringGenerator(_mockedNumberGenerator.Object);
        }

        [Test]
        public void GenerateEvenOddPairs_ShouldReturnAListWithPairsOfNumbers_WhenTheNumbersArePositive()
        {
            //act
            var result=_stringGenerator.GenerateEvenOddPairs(2, 10);

            Assert.That(result,Is.EquivalentTo(new List<string>() {"2,9","2,9"}));
        }

        [TestCase(0)]
        [TestCase(-2)]
        public void GenerateEvenOddPairs_ShouldReturnArgumentOutOfRangeException_WhenPairCountIsLessThanOne(int pairCount)
        {
            //assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _stringGenerator.GenerateEvenOddPairs(pairCount, 10));

        }
    }
}
