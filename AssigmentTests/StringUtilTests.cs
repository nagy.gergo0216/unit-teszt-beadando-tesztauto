﻿using System;
using Assignment.Strings;
using NUnit.Framework;



namespace AssigmentTests
{
    [TestFixture()]
    public class StringUtilTests
    {
        private StringUtil _stringUtil;

        [OneTimeSetUp]
        public void Setup()
        {
            _stringUtil = new StringUtil();
        }

        [Test]
        public void IsPalindrom_ShouldReturnFalse_WhenStringIsNotPalindrom()
        {
            //act
            var result = _stringUtil.IsPalindrom("barcelona");
            //assert
            Assert.IsFalse(result);
        }

        [TestCase("ABBA")]
        [TestCase("Hannah")]
        public void IsPalindrom_ShouldReturnTrue_WhenStringIsPalindrom(string str)
        {
            //act
            var result = _stringUtil.IsPalindrom(str);

            //assert
            Assert.IsTrue(result);
        }

        [TestCase("")]
        [TestCase("a")]
        public void IsPalindrom_ShouldReturnFalse_WhenStringLengthIsLessThanTwo(string str)
        {
            var result = _stringUtil.IsPalindrom(str);

            Assert.IsFalse(result);
        }
    }
}
