﻿using System;
using Assignment.Numbers;
using System.Collections.Generic;

namespace Assignment.Strings
{
    public class StringGenerator
    {
        private readonly INumberGenerator _numberGenerator;

        public StringGenerator(INumberGenerator numberGenerator)
        {
            _numberGenerator = numberGenerator;
        }

        /// <summary>
        /// Generates pairs of even and odd numbers.
        /// </summary>
        /// <param name="pairCount">Number of items to generate.</param>
        /// <param name="max">Maximum value of numbers generated.</param>
        /// <returns>A list of strings, containing an even and an odd positive number, separated by a comma (,). 
        /// All numbers are in the range of [0..max]</returns>
        public List<string> GenerateEvenOddPairs(int pairCount, int max)
        {

            //pairCount ellenőrzés hozzáírása

            if (pairCount >= 1)
            {
                var evenOddPairs = new List<string>();

                //for ciklus 1-től indult, így viszont csak 1 pár jött létre
                for (int i = 0; i < pairCount; i++)
                {
                    var element = _numberGenerator.GenerateEven(max) + "," + _numberGenerator.GenerateOdd(max);
                    evenOddPairs.Add(element);
                }

                return evenOddPairs;
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}
