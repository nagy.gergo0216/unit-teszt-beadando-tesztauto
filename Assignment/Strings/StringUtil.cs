﻿using System;
using System.Linq;
using System.Text;

/**
 * https://hu.wikipedia.org/wiki/Palindrom
 */
namespace Assignment.Strings
{
    //az osztaly private volt
    public class StringUtil
    {
        /// <summary>
        /// Checks if the string passed in is a palindrom or not.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>True if the passed in string is a palindrom, otherwise false.</returns>
        public bool IsPalindrom(string str)
        {
            if (str.Length > 1)
            {
                //var reverse = new StringBuilder(str).ToString().Reverse<char>().ToString();
                var reverse = new StringBuilder(str).ToString().Reverse().ToArray();
                //str.Equals(str) volt str.Equals(new string(reverse)) helyett.

                return str.ToLower().Equals(new string(reverse).ToLower());
            }
            return false;

        }
    }
}
